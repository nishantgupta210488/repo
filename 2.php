<?php 
public function add_talent($id = NULL, $start = NULL) {
		//echo '1'; die;
		
		$this->_checkSession();
		$this->layout = "admin_layout";
		$this->set('title_for_layout', SITE_TITLE.' :: Talent');
		$this->set('page_heading', 'Add Talent');
		
		//Drop Down List Fetching Start
		$state_details = $this->State->find( 'list' , array('fields' => array('State.state_name') , 'conditions' => array( 'State.isblocked !=' => 1, 'State.isdeleted !=' => 1), 'order' => array('State.state_name' => 'ASC') ));   
		$this->set( 'state_details', $state_details );


		$all_city = $this->City->find( 'list' , array('fields' => array('City.city_name') , 'conditions' => array( 'City.isblocked !=' => 1, 'City.isdeleted !=' => 1), 'order' => array('City.city_name' => 'ASC') ));   
		$this->set( 'all_city', $all_city );
		
		$ethnicity_details = $this->Ethnicity->find( 'list' , array('fields' => array('Ethnicity.ethnicity_name') , 'conditions' => array( 'Ethnicity.isblocked !=' => 1, 'Ethnicity.isdeleted !=' => 1), 'order' => array('Ethnicity.ethnicity_name' => 'ASC') ));   
		$this->set( 'ethnicity_details', $ethnicity_details );
		
		$hair_list = $this->Hair->find( 'list' , array('fields' => array('Hair.hairlist') , 'conditions' => array( 'Hair.isblocked !=' => 1, 'Hair.isdeleted !=' => 1), 'order' => array('Hair.id' => 'ASC') ));   
		$this->set( 'hair_list', $hair_list );
		
		$eye_details = $this->Eye->find( 'list' , array('fields' => array('Eye.eye_name') , 'conditions' => array( 'Eye.isblocked !=' => 1, 'Eye.isdeleted !=' => 1), 'order' => array('Eye.id' => 'ASC') ));   
		$this->set( 'eye_details', $eye_details );
		
		$education_list = $this->Education->find( 'list' , array('fields' => array('Education.education_type') , 'conditions' => array( 'Education.isblocked !=' => 1, 'Education.isdeleted !=' => 1), 'order' => array('Education.id' => 'ASC') ));   
		$this->set( 'education_list', $education_list );

		//promotional list edit
		$promotional_data = array();
		$promotional_list = $this->PromotionalInterest->find( 'list' , array('fields' => array('PromotionalInterest.promotionallist') , 'conditions' => array( 'PromotionalInterest.isdeleted !=' => 1, 'PromotionalInterest.isdisplay !=' => 0, 'PromotionalInterest.isblocked !=' => 1), 'order' => array('PromotionalInterest.promotionallist' => 'ASC') ));
		foreach ($promotional_list as $key => $promotional_value) {
			$promotional_data[$promotional_value] = $promotional_value;
		}
		//pr($promotional_data); die;  
		$this->set( 'promotional_data', $promotional_data ); 
		
		$size_list = $this->Size->find( 'list' , array('fields' => array('Size.size_type') , 'conditions' => array( 'Size.isblocked !=' => 1, 'Size.isdeleted !=' => 1), 'order' => array('Size.id' => 'ASC') ));   
		$this->set( 'size_list', $size_list );
		
		//Pennsylvania =38
		$city_list = $this->City->find( 'all' , array('fields' => array('State.id', 'State.state_name', 'City.id', 'City.state_id', 'City.city_name') , 'conditions' => array( 'City.isblocked !=' => 1, 'City.isdeleted !=' => 1, 'State.isblocked !=' => 1, 'State.isdeleted !=' => 1), 'order' => array('City.id' => 'ASC') )); 
		//pr($city_list); die;  
		$this->set( 'city_list', $city_list );
		
		//enhancement left join
		$state_city_details = $this->State->find('all', array(
			'joins' => array(
				array(
				'table' => 'es_cities',
				'alias' => 'City',
				'type' => 'Left',
					'conditions' => array(
					'City.state_id = State.id'
					)
				)
			),
				'conditions' => array(
				'State.isblocked !=' => 1, 'State.isdeleted !=' => 1, 'City.isblocked !=' => 1, 'City.isdeleted !=' => 1
				),
				'fields' => array('State.id', 'State.state_name', 'City.id', 'City.state_id', 'City.city_name'),
				'order' => array('State.state_name' => 'ASC')
		));
			//pr($state_city_details); die;
		$this->set( 'state_city_details', $state_city_details );

		$current_date = date('d-m-Y');
		//echo $current_date;die('testing...');
		$this->set('current_date',$current_date);
		//echo '1'; die;
		
		$talentMultiImg = array();
		//print_r($talentMultiImg);
		//echo '2'; die;
		$this->set( 'talentMultiImg', $talentMultiImg );

		//Drop Down List Fetching End
		$this->Session->write('startPage', $start);
	//echo '<pre>'; print_r($this->request->data); die;
		if(!empty($this->request->data))
		{
			//echo '1'; die;
			//Setting Default Value For Integer Field//
			if(!empty($this->request->data['Talent']['ethnicity_id']))
			{
				$this->request->data['Talent']['ethnicity_id'] = $this->request->data['Talent']['ethnicity_id'];
			}
			else
			{
				$this->request->data['Talent']['ethnicity_id'] = 0;
			}
			
			if(!empty($this->request->data['Talent']['state_id']))
			{
				$this->request->data['Talent']['state_id'] = $this->request->data['Talent']['state_id'];
			}
			else
			{
				$this->request->data['Talent']['state_id'] = 0;
			}
			
			if(!empty($this->request->data['Talent']['education_id']))
			{
				$this->request->data['Talent']['education_id'] = $this->request->data['Talent']['education_id'];
			}
			else
			{
				$this->request->data['Talent']['education_id'] = 0;
			}
			
			if(!empty($this->request->data['Talent']['eye_id']))
			{
				$this->request->data['Talent']['eye_id'] = $this->request->data['Talent']['eye_id'];
			}
			else
			{
				$this->request->data['Talent']['eye_id'] = 0;
			}
			
			if(!empty($this->request->data['Talent']['hair_id']))
			{
				$this->request->data['Talent']['hair_id'] = $this->request->data['Talent']['hair_id'];
			}
			else
			{
				$this->request->data['Talent']['hair_id'] = 0;
			}
			
			if(!empty($this->request->data['Talent']['pants_id']))
			{
				$this->request->data['Talent']['pants_id'] = $this->request->data['Talent']['pants_id'];
			}
			else
			{
				$this->request->data['Talent']['pants_id'] = 0;
			}
			
			if(!empty($this->request->data['Talent']['shirts_id']))
			{
				$this->request->data['Talent']['shirts_id'] = $this->request->data['Talent']['shirts_id'];
			}
			else
			{
				$this->request->data['Talent']['shirts_id'] = 0;
			}
			//End Of Assigning Default Value For Integer Field//
			//echo '<pre>'; print_r($this->request->data); die;

			if($this->request->data['Talent']['editid'] == 0)
			{
				//echo 'running'; die; 
				if($this->Talent->save($this->request->data)) {
				//echo 'out'; die;	
					if(@$this->request->data['Talent']['id']=='')
						$id = $this->Talent->getLastInsertID();
					else
						$id = $this->request->data['Talent']['id'];
					// For Creating Unique Talent Code Using The Last Name's First Character Of A Person	
					#####Evon#####
					//echo $id; die;
                                        //For Grade Assining
					$GradeAssign = array();
					$GradeAssign = $this->request->data;
					$GradeAssign['TalentGrade']['grade_id'] = 1;
					$GradeAssign['TalentGrade']['talent_id'] = $id;
					$this->request->data = $GradeAssign;
					$this->TalentGrade->save($this->request->data);
					##############
					$extdata = array();
					$extdata = $this->request->data;
					$Generate_Code = rand(0,9999999);
					$FrstName = ucfirst($extdata['Talent']['first_name']);
					$Passwd   = $extdata['Talent']['password'];
					$DOB = date('Y-m-d', strtotime($this->request->data['Talent']['birth']));
					
					$extdata['Talent']['talent_code'] = "RTN".$Generate_Code;
					$extdata['Talent']['password']    = base64_encode($Passwd);
					$extdata['Talent']['birth']       = $DOB;
					$extdata['Talent']['isblocked']   = 1;
					$this->request->data = $extdata;
					$this->Talent->save($this->request->data);
					// End Of Implementing Talent Code Using The Last Name's First Character Of A Person
					
					//For Image Upload
					//echo '<pre>'; print_r($this->request->data); die;

					if($this->request->data['TalentImage']['image_upload']['name'] != '') {
						
						$imagename = $this->request->data['TalentImage']['image_upload']['name'];
						$imagetype = $this->request->data['TalentImage']['image_upload']['type'];			
						$type = array('image/png', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/gif', 'image/x-png');
						
						if(!in_array($imagetype, $type)) {
							$this->Session->setFlash('Due To Invalid Type Image Is Not Uploaded.');
							$this->redirect(array('controller' => 'Talents', 'action' => 'add_talent'));
						}
						//echo '1'; die;
						
						$uploadimage = 'Talent-'.$id.'-'.time().$imagename;
						//print_r($uploadimage); die;
						$TargetImage = 'img/upload/talent-image/original/'.$uploadimage;
						move_uploaded_file($this->request->data['TalentImage']['image_upload']['tmp_name'], $TargetImage); 
						$destination_1 = 'img/upload/talent-image/50x50/'.$uploadimage;
						$destination_2 = 'img/upload/talent-image/130x160/'.$uploadimage;
						$destination_3 = 'img/upload/talent-image/153x112/'.$uploadimage;
						$destination_4 = 'img/upload/talent-image/308x247/'.$uploadimage;
						
						$this->imgOptCpy($TargetImage, $destination_1, 50, 50, 100, false);
						$this->imgOptCpy($TargetImage, $destination_2, 130, 160, 100, false);
						$this->imgOptCpy($TargetImage, $destination_3, 153, 112, 100, false);
						$this->imgOptCpy($TargetImage, $destination_4, 308, 247, 100, false);
						
						$this->request->data['TalentImage']['talent_id'] = $id;
						$this->request->data['TalentImage']['image_name'] = $uploadimage;
						$this->TalentImage->save($this->request->data);
						$this->multipleupload($id, $this->request->data);
					
					} else {
						//echo '1'; die;
						$this->Session->setFlash('Please Upload The Talent Image.');
						$this->redirect(array('controller' => 'Talents', 'action' => 'add_talent'));
					}
					//End Of Image Upload
					
					//For Resume Upload

					if($this->request->data['Talent']['resume_upload']['name']!=null)
					{
						$filename = $this->request->data['Talent']['resume_upload']['name'];
						
						$newname = explode(".", $filename);

						$fileExt=end($newname);
					        $ext = pathinfo($filename, PATHINFO_EXTENSION);
						$resume  = $FrstName."-".$id."-Resume-".time().".".$ext;
						
						$destination  = 'img/upload/talent_resume/';
						$file         = $this->request->data['Talent']['resume_upload'];
						
						$newname = 'img/upload/talent_resume/'.$resume;					    
						move_uploaded_file($this->request->data['Talent']['resume_upload']['tmp_name'], $newname);
						
						$this->request->data['Talent']['resume'] = $resume;
						$this->Talent->save($this->request->data);
					}
					//End Of Resume Upload
					//echo '2'; die;
					//For Multiple Table Value Insert
					$talentid = $id;
					
					for($i=0; $i < count($this->request->data['TalentAvailable']['avail_day']); $i++) 
					{
						if(!empty($this->request->data['TalentAvailable']['avail_day'][$i])){
						
							$avail_data['TalentAvailable']['talent_id'] = $talentid;
							$avail_data['TalentAvailable']['avail_day'] = $this->request->data['TalentAvailable']['avail_day'][$i];
							
							$this->TalentAvailable->create();
							$avail_data['TalentAvailable']['id']="";
							$this->TalentAvailable->save($avail_data['TalentAvailable']);
						}
					}
					
					for($i=0; $i < count($this->request->data['TalentLanguage']['languagetype']); $i++) 
					{
						if(!empty($this->request->data['TalentLanguage']['languagetype'][$i])){
						
							$langData['TalentLanguage']['talent_id'] = $talentid;
							$langData['TalentLanguage']['languagetype'] = $this->request->data['TalentLanguage']['languagetype'][$i];
							
							$this->TalentLanguage->create();
							$langData['TalentLanguage']['id']="";
							$this->TalentLanguage->save($langData['TalentLanguage']);
						}
					}
					
					for($i=0; $i < count($this->request->data['TalentRegion']['city_name']); $i++)
					{
						if(!empty($this->request->data['TalentRegion']['city_name'][$i])){
						
							$regionData['TalentRegion']['talent_id'] = $talentid;
							//$regionData['TalentRegion']['state_id']  = $this->request->data['TalentRegion']['state_id'];
							$regionData['TalentRegion']['city_name'] = $this->request->data['TalentRegion']['city_name'][$i];
							
							$this->TalentRegion->create();
							$regionData['TalentRegion']['id'] = "";
							$this->TalentRegion->save($regionData['TalentRegion']);
						}
					}
					
					for($i=0; $i < count($this->request->data['TalentRole']['roletype']); $i++)
					{
						if(!empty($this->request->data['TalentRole']['roletype'][$i])){
						
							$roleData['TalentRole']['talent_id'] = $talentid;
							$roleData['TalentRole']['roletype'] = $this->request->data['TalentRole']['roletype'][$i];
							
							$this->TalentRole->create();
							$roleData['TalentRole']['id']="";
							$this->TalentRole->save($roleData['TalentRole']);
						}
					}
					//End Of Multiple Table Value Insert
					
					$this->Session->setFlash('<font color="green">Data added successfully.</font>');
					$this->redirect(array('controller' => 'Talents', 'action' => 'list_all'));
					
				} else {
					$this->Session->setFlash('<font color="red">Error occurred. Please try again.</font>');
					$this->redirect(array('controller' => 'Talents', 'action' => 'add_talent'));
				}
			}
			else
			{
				//echo '1'; die;
				$this->Talent->id = $this->request->data['Talent']['editid'];
				$talentid = base64_decode($id);
				
				//pr($this->request->data);exit;
				

				if($this->Talent->save($this->request->data)) {
					
					// For Creating Unique Talent Code Using The Last Name's First Character Of A Person
					$extdata = array();
					$extdata = $this->request->data;
					$FrstName = ucfirst($extdata['Talent']['first_name']);
					$Passwd   = $extdata['Talent']['password'];
					$DOB = date('Y-m-d', strtotime($this->request->data['Talent']['birth']));
					
					$extdata['Talent']['password'] = base64_encode($Passwd);
					$extdata['Talent']['birth'] = $DOB;
					$this->request->data = $extdata;
					$this->Talent->save($this->request->data);
					// End Of Implementing Talent Code Using The Last Name's First Character Of A Person
					//echo '2'; die;
					//echo '<pre>'; print_r($this->request->data); die;
					//For Image Upload
					if(!empty($this->request->data['NewTalentImage'])) {
						//echo '1'; die;
						$data['TalentImage'] = $this->request->data['NewTalentImage'];
						//pr($data['TalentImage']);
						//unset($this->request->data['NewTalentImage']);
						
						//pr($data);exit('ttt');	
						$this->multipleupload($talentid, $data);
					} //die;
					/*if($this->request->data['TalentImage']['image_upload']['name'] != '') {
						
						$imagename = $this->request->data['TalentImage']['image_upload']['name'];
						$imagetype = $this->request->data['TalentImage']['image_upload']['type'];			
						$type = array('image/png', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/gif', 'image/x-png');
						
						if(!in_array($imagetype,$type)) {
							$this->Session->setFlash('Due To Invalid Type Image Is Not Uploaded.');
							$this->redirect(array('controller' => 'Talents', 'action' => 'add_banner'));
						}
						
						$uploadimage = 'Talent-'.$talentid.'-'.time().$imagename;
						$TargetImage = 'img/upload/talent-image/original/'.$uploadimage;
						move_uploaded_file($this->request->data['TalentImage']['image_upload']['tmp_name'], $TargetImage);
						//@unlink('img/upload/talent-image/original/'.$this->request->data['TalentImage']['hidden_image']); 
						
						$destination_1 = 'img/upload/talent-image/50x50/'.$uploadimage;	
						$this->imgOptCpy($TargetImage, $destination_1, 50, 50, 100, false);
						//@unlink('img/upload/talent-image/50x50/'.$this->request->data['TalentImage']['hidden_image']);
						
						$destination_2 = 'img/upload/talent-image/130x160/'.$uploadimage;
						$this->imgOptCpy($TargetImage, $destination_2, 130, 160, 100, false);
						//@unlink('img/upload/talent-image/130x160/'.$this->request->data['TalentImage']['hidden_image']);
						
						$destination_3 = 'img/upload/talent-image/153x112/'.$uploadimage;
						$this->imgOptCpy($TargetImage, $destination_3, 153, 112, 100, false);
						//@unlink('img/upload/talent-image/153x112/'.$this->request->data['TalentImage']['hidden_image']);
						
						$destination_4 = 'img/upload/talent-image/308x247/'.$uploadimage;
						$this->imgOptCpy($TargetImage, $destination_4, 308, 247, 100, false);
						//@unlink('img/upload/talent-image/308x247/'.$this->request->data['TalentImage']['hidden_image']);
						
						$this->request->data['TalentImage']['talent_id']  = $talentid;
						$this->request->data['TalentImage']['image_name'] = $uploadimage;
						$this->TalentImage->save($this->request->data);
						$this->multipleupload($talentid, $this->request->data);
						
					}*/
					//End Of Image Upload
					
					//For Resume Upload
					if($this->request->data['Talent']['resume_upload']['name']!=null)
					{
						$filename = $this->request->data['Talent']['resume_upload']['name'];
						
						$ext = pathinfo($filename, PATHINFO_EXTENSION);
						$resume  = $FrstName."-".$talentid."-Resume-".time().".".$ext;
						
						$destination  = 'img/upload/talent_resume/';
						$file         = $this->request->data['Talent']['resume_upload'];
						
						$newname = 'img/upload/talent_resume/'.$resume;					    
						move_uploaded_file($this->request->data['Talent']['resume_upload']['tmp_name'], $newname);
						
						$this->request->data['Talent']['resume'] = $resume;
						$this->Talent->updateAll(array('Talent.resume' => "'".$this->request->data['Talent']['resume']."'"), array('Talent.id' => base64_decode($id)));
					}
					//End Of Resume Upload
					
					//For Multiple Table Value Insert
					for($i=0; $i < count($this->request->data['TalentAvailable']['avail_day']); $i++) 
					{
						if(!empty($this->request->data['TalentAvailable']['avail_day'][$i])){
						
							$avail_data['TalentAvailable']['talent_id'] = $talentid;
							$avail_data['TalentAvailable']['avail_day'] = $this->request->data['TalentAvailable']['avail_day'][$i];
							
							$this->TalentAvailable->create();
							$avail_data['TalentAvailable']['id']="";
							$this->TalentAvailable->save($avail_data['TalentAvailable']);
						}
					}
					
					for($i=0; $i < count($this->request->data['TalentLanguage']['languagetype']); $i++) 
					{
						if(!empty($this->request->data['TalentLanguage']['languagetype'][$i])){
						
							$langData['TalentLanguage']['talent_id'] = $talentid;
							$langData['TalentLanguage']['languagetype'] = $this->request->data['TalentLanguage']['languagetype'][$i];
							
							$this->TalentLanguage->create();
							$langData['TalentLanguage']['id']="";
							$this->TalentLanguage->save($langData['TalentLanguage']);
						}
					}
					$TalentRegionData = $this->TalentRegion->find('all', array('conditions' => array( 'TalentRegion.talent_id' => $talentid), 'fields' => array( 'TalentRegion.id')));
					$TalRegCnt = count($TalentRegionData);

					for($i=0; $i < count($this->request->data['TalentRegion']['city_name']); $i++)
					{
						if(!empty($this->request->data['TalentRegion']['city_name'][$i])){
						        
                                                        for($j=0; $j<$TalRegCnt; $j++){
								$TalRegId = $TalentRegionData[$j]['TalentRegion']['id'];
								$this->TalentRegion->delete($TalRegId);
								
							}
							$regionData['TalentRegion']['talent_id'] = $talentid;
							//$regionData['TalentRegion']['state_id']  = $this->request->data['TalentRegion']['state_id'];
							$regionData['TalentRegion']['city_name'] = $this->request->data['TalentRegion']['city_name'][$i];
							
							$this->TalentRegion->create();
							$regionData['TalentRegion']['id'] = "";
							$this->TalentRegion->save($regionData['TalentRegion']);
						}
					}
					
					for($i=0; $i < count($this->request->data['TalentRole']['roletype']); $i++)
					{
						if(!empty($this->request->data['TalentRole']['roletype'][$i])){
						
							$roleData['TalentRole']['talent_id'] = $talentid;
							$roleData['TalentRole']['roletype'] = $this->request->data['TalentRole']['roletype'][$i];
							
							$this->TalentRole->create();
							$roleData['TalentRole']['id']="";
							$this->TalentRole->save($roleData['TalentRole']);
						}
					}
					//End Of Multiple Table Value Insert
					
					$this->Session->setFlash('<font color="green">Data edited successfully.</font>');
					$this->redirect(array('controller' => 'Talents','action' => 'list_all/'.$start.'/'));
					
				} else {
					$this->Session->setFlash('<font color="red">Error occurred.. Please try again..</font>');
					$this->redirect(array('controller' => 'Talents', 'action' => 'add_talent'));
				}
			}			
		}	
		if($id != null)
		{
			$this->set('page_heading', 'Edit Talent');
			$id = base64_decode($id);
			$this->Talent->id = $id;
			$this->request->data = $this->Talent->read();
			$this->set('id', $id);
			//echo '<pre>'.$this->request->data['Talent']['password'].'</pre>';exit;
			// Start Of Multiple Table Value Fetching
			$langType = $this->TalentLanguage->find( 'list', array('conditions' => array( 'TalentLanguage.talent_id' => $id ), 'fields' => array( 'TalentLanguage.id','TalentLanguage.languagetype' ) ));
			$this->set('langType', $langType);
			
			$dayCount = $this->TalentAvailable->find( 'list', array('conditions' => array( 'TalentAvailable.talent_id' => $id ), 'fields' => array( 'TalentAvailable.id','TalentAvailable.avail_day' ) ));
			$this->set('dayCount', $dayCount);
			
			$roleType = $this->TalentRole->find( 'list', array('conditions' => array( 'TalentRole.talent_id' => $id ), 'fields' => array( 'TalentRole.id','TalentRole.roletype' ) ));
			$this->set('roleType', $roleType);
			
			//$regionType = $this->TalentRegion->find( 'list', array('conditions' => array( 'TalentRegion.talent_id' => $id ), 'fields' => array( 'TalentRegion.id' ) ));
			//$this->set('regionType', $regionType);
			
			$regionType = $this->TalentRegion->find( 'list', array('conditions' => array( 'TalentRegion.talent_id' => $id ), 'fields' => array( 'TalentRegion.id','TalentRegion.city_name' ) ));
			$this->set('regionType', $regionType);
			
			$talentMultiImg = $this->TalentImage->find( 'all', array('conditions' => array('TalentImage.talent_id' => $id), 'fields' => array('TalentImage.*') ));
			$this->set( 'talentMultiImg', $talentMultiImg );
			// End Of Multiple Table Value Fetching
		}
	}

	

	Bbscore.com
	artpxmedia
	Alesanz#1988

	cpanel-> https://p3plcpnl0917.prod.phx3.secureserver.net:2083/
	go daady-> https://in.godaddy.com/


vjrk => superadmin/admin123

green warrior admin:
img missing on top admin panel
chnges admin docs Arry when we sue file in asmin baesuty and beasy
change in API accestokn use social login API
push code folder to green warrir folder and check

update code on git

log file where write check